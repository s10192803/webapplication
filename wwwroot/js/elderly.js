﻿function elderlyResize() {

    var success = check();

    if (success == false) {
        for (var i = 1; i < document.getElementsByTagName("label").length; i++) {
            document.getElementsByTagName("label")[i].setAttribute("style", "font-size:45px;");
        }
        for (var i = 1; i < document.getElementsByTagName("input").length; i++) {
            document.getElementsByTagName("input")[i].setAttribute("style", "font-size:40px;");
        }
        document.getElementsByClassName("gender")[0].setAttribute("style", "width:200px !important; font-size:45px;");
        document.getElementsByClassName("gender")[1].setAttribute("style", "width:200px !important; font-size:45px;");
    }
    else {
        for (var i = 1; i < document.getElementsByTagName("label").length; i++) {
            document.getElementsByTagName("label")[i].setAttribute("style", "font-size:18px;");
        }
        for (var i = 1; i < document.getElementsByTagName("input").length; i++) {
            document.getElementsByTagName("input")[i].setAttribute("style", "font-size:18px;");
        }
        document.getElementsByClassName("gender")[0].setAttribute("style", "width:100px !important; font-size:18px;");
        document.getElementsByClassName("gender")[1].setAttribute("style", "width:100px !important; font-size:18px;");
    }
    

}

function check() {
    return Boolean(document.getElementById("gender").style.fontSize == "45px");
}