﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class Customer : User
    {
        [Display(Name = "Address")]
        [StringLengthAttribute(50)]
        public string? Address { get; set; }

        [Display(Name = "Medical history")]
        public string? MedicalHistory { get; set; }

        [Display(Name = "Dietary")]
        [StringLengthAttribute(255)]
        public string? Dietary { get; set; }

        //[Display(Name = "E-Wallet")]
        //public int? eWallet { get; set; }
    }
}
