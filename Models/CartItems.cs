﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class CartItems
    {
        [Display(Name = "Customer ID")]
        public int CustomerID { get; set; }

        [Display(Name = "ItemID")]
        public int ItemID { get; set; }

        [Display(Name = "Quantity")]
        public int qty { get; set; }

    }
}
