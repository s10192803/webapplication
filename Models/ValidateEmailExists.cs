﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;
using WebApplication.DAL;

namespace WebApplication.Models
{
    public class ValidateEmailExists : ValidationAttribute
    {
        private CustomerDAL customerContext = new CustomerDAL();
        private VolunteerDAL volunteerContext = new VolunteerDAL();

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            //Get the email value to validate
            string email = Convert.ToString(value);
            //Casting the validation context to the "User" model class
            User user = (User)validationContext.ObjectInstance;

            if (customerContext.ValidateCustomerEmail(email) == true ||
                volunteerContext.ValidateVolunteerEmail(email) == true)
            {
                //validation failed
                return new ValidationResult("Email address already registered!");
            }
            else
            {
                //validation passed
                return ValidationResult.Success;
            }


        }
    }
}
