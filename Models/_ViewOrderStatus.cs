﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class _ViewOrderStatus
    {
        public int OrderID { get; set; }
        public string Status { get; set; }
        public decimal TotalPrice { get; set; }
        public List<Item> ItemList { get; set; }
        public List<CartItems> OrderItem { get; set; }
        public DateTime? DeliveryDateTime { get; set; }
    }
}
