﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class _ViewOrderHistory
    {
        public int OrderID { get; set; }
        public List<Item> ItemList { get; set; }
        public List<CartItems> OrderItem { get; set; }
        public string Status { get; set; }
        public DateTime OrderCreated { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
