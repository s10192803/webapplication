﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class Item
    {
        [Display(Name = "ItemID")]
        public int ItemID { get; set; }

        [Display(Name = "Name")]
        [StringLengthAttribute(255)]
        public string Name { get; set; }

        [Display(Name = "Status")]
        [StringLengthAttribute(255)]
        public string Status { get; set; }

        [Display(Name = "Price")]
        public decimal Price { get; set; }

        [Display(Name = "Category")]
        [StringLengthAttribute(255)]
        public string Category { get; set; }

        [Display(Name = "Dietary")]
        [StringLengthAttribute(255)]
        public string? Dietary { get; set; }

        [Display(Name = "StoreName")]
        [StringLengthAttribute(255)]
        public string? StoreName { get; set; }
    }
}
