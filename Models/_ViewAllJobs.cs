﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class _ViewAllJobs
    {
        public List<Order> OrderList { get; set; }
        public List<Customer> CustomerList { get; set; }
        public List<CartItems> OrderItemList { get; set; }
        public List<Item> ItemList { get; set; }
    }
}
