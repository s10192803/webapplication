﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class Store
    {
        [Display(Name = "StoreID")]
        public int ID { get; set; }

        [Display(Name = "StoreName")]
        [StringLengthAttribute(255)]
        public string StoreName { get; set; }

        [Display(Name = "Store Area")]
        [StringLengthAttribute(255)]
        public string Area { get; set; }

        [Display(Name = "Address")]
        [StringLengthAttribute(255)]
        public string Address { get; set; }
    }
}
