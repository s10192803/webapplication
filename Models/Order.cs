﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class Order
    {
        [Display(Name = "Order ID")]
        public int OrderID { get; set; }

        [Display(Name = "CustomerID")]
        public int CustomerID { get; set; }

        [Display(Name = "VolunteerID")]
        public int? VolunteerID { get; set; }

        [Display(Name = "Status")]
        [StringLengthAttribute(255)]
        public string Status { get; set; }

        [Display(Name = "Remarks")]
        [StringLengthAttribute(255)]
        public string? Remarks { get; set; }

        [Display(Name = "DeliveryDateTime")]
        [DataType(DataType.Date)]
        public DateTime? DelieveryDateTime { get; set; }

        [Display(Name = "OrderCreated")]
        [DataType(DataType.Date)]
        public DateTime OrderCreated { get; set; }
        [Display(Name = "Total Cost")]
        [StringLengthAttribute(255)]
        public decimal TotalCost { get; set; }
    }
}
