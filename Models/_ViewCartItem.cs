﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class _ViewCartItem
    {
        public List<CartItems> CartItemList  { get; set; }

        public List<Item> ItemList { get; set; }

        public decimal TotalPrice { get; set; }
    }
}
