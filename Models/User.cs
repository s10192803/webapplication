﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class User
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "This property is required")]
        [RegularExpression(@"^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Please enter proper format for name.")]
        [StringLengthAttribute(50)]
        public string Name { get; set; }

        [Display(Name = "Email Address")]
        [ValidateEmailExists]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}", ErrorMessage = "Please enter a proper format for email.")]
        public string EmailAddr { get; set; }

        [Display(Name = "Date of birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime DOB { get; set; }

        [Display(Name = "Phone Number")]
        [RegularExpression(@"[6|8|9]+[0-9]{7}", ErrorMessage = "Please enter a valid phone number.")]
        public string PhoneNum { get; set; }

        [Display(Name = "Gender")]
        [StringLengthAttribute(1)]
        public string? Gender { get; set; }

        [Display(Name = "Password")]
        [StringLengthAttribute(255)]
        public string Password { get; set; }


        //Messages for sprint 2 or so 
        //[Display(Name = "Messages")]
        //public List<String>? Messages { get; set; }
    }
}
