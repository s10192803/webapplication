﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class Staff : User
    {
        [Display(Name = "Role")]
        [Required(ErrorMessage = "This property is required")]
        public string Role { get; set; }
    }
}
