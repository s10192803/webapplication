﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication.Models;

using System.Net.Http;
//using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
//using Newtonsoft.Json;
using WebApplication.DAL;

namespace WebApplication.Controllers
{
    public class AdminController : Controller
    {
        //DAL
        //=======================================================================================
        VolunteerDAL VolunteerContext = new VolunteerDAL();
        CustomerDAL CustomerContext = new CustomerDAL();
        AdminDAL AdminContext = new AdminDAL();
        FoodDAL FoodContext = new FoodDAL();
        GroceryDAL GroceryContext = new GroceryDAL();
        //=======================================================================================

        public IActionResult ViewCustomerList()
        {
            List<Customer> customerList = new List<Customer>();
            customerList = CustomerContext.GetAllCustomer();
            return View(customerList);
        }

        //=======================================================================================
        public IActionResult ViewVolunteerList()
        {
            List<Staff> staffList = new List<Staff>();
            staffList = VolunteerContext.GetAllVolunteer();
            return View(staffList);
        }
        //=======================================================================================
        public ActionResult Index()
        {
            return View();
        }
        //=======================================================================================
        public ActionResult AdminEditCustomer(int id)
        {
            if(HttpContext.Session.GetString("Role")== "Admin")
            {
                Customer customer = AdminContext.GetCustomerDetails(id);
                return View(customer);
            }
            return View();
        }
        //=======================================================================================
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminEditCustomer(Customer customer)
        {
            if (HttpContext.Session.GetString("Role") == "Admin")
            {
                CustomerContext.Edit(customer);
                return RedirectToAction("ViewCustomerList", "Admin");
            }
            else if (HttpContext.Session.GetString("Role") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", HttpContext.Session.GetString("Role"));
            }
        }
        //=======================================================================================
        public ActionResult AdminEditVolunteer(int id)
        {
            if (HttpContext.Session.GetString("Role") == "Admin")
            {
                Staff Volunteer = AdminContext.GetVolunteerDetails(id);
                return View(Volunteer);
            }
            return View();
        }
        //=======================================================================================
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminEditVolunteer(Staff volunteer)
        {
            if (HttpContext.Session.GetString("Role") == "Admin")
            {
                VolunteerContext.Edit(volunteer);
                return RedirectToAction("ViewVolunteerList", "Admin");
            }
            else if (HttpContext.Session.GetString("Role") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", HttpContext.Session.GetString("Role"));
            }
        }

        public ActionResult ViewGrocery()
        {
            List<Item> GroceryList = new List<Item>();
            GroceryList = GroceryContext.GetItems();
            return View(GroceryList);
        }

        public ActionResult DeleteGroceryItem(int itemid)
        {
            AdminContext.DeleteFoodItem(itemid);

            return RedirectToAction("ViewGrocery");
        }

        public ActionResult EditGrocery(int id)
        {
            ViewData["options"] = GetStatusOptions();
            Item item = AdminContext.GetItem(id);
            return View(item);
        }

        public ActionResult UpdateGroceryItem(Item item)
        {
            AdminContext.UpdateItem(item);

            return RedirectToAction("ViewGrocery");
        }

        public ActionResult AddGrocery()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddGrocery(Item grocery)
        {
            int itemID = AdminContext.AddGrocery(grocery);
            TempData["Success"] = "Item added successfully!";
            return RedirectToAction("AddGrocery");
        }

        public ActionResult AddStore()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddStore(Store store)
        {
            int storeID = AdminContext.AddStore(store);
            TempData["Success"] = "Store added successfully!";

            return View();
        }

        public ActionResult ViewStores()
        {
            List<Store> storeList = AdminContext.GetStores();
            return View(storeList);
        }

        public ActionResult StoreItems(int? id)
        {
            List<Item> FoodList = new List<Item>();
            string StoreName = FoodContext.GetStore(Convert.ToInt32(id)).StoreName;
            FoodList = FoodContext.GetFoodItems(StoreName);
            TempData["StoreName"] = StoreName;
            TempData["StoreID"] = id;
            HttpContext.Session.SetInt32("StoreID", (int)id);
            return View(FoodList);
        }

        public ActionResult AddFoodItem(int? id)
        {
            TempData["StoreID"] = id;
            return View();
        }

        [HttpPost]
        public ActionResult AddFoodItem(Item item, int id)
        {
            string StoreName = FoodContext.GetStore(id).StoreName;
            item.StoreName = StoreName;
            int itemID = AdminContext.AddFoodItem(item);
            return RedirectToAction("StoreItems", new { id = id });
        }

        public ActionResult DeleteFoodItem(int itemid)
        {
            AdminContext.DeleteFoodItem(itemid);
            int storeID = (int)HttpContext.Session.GetInt32("StoreID");
            return RedirectToAction("StoreItems", new { id = storeID });
        }

        public ActionResult EditFoodItem(int id)
        {
            ViewData["options"] = GetStatusOptions();
            Item item = AdminContext.GetItem(id);
            return View(item);
        }

        public ActionResult UpdateFoodItem(Item item)
        {
            AdminContext.UpdateItem(item);
            int storeID = (int)HttpContext.Session.GetInt32("StoreID");

            return RedirectToAction("StoreItems", new { id = storeID });
        }

        private List<SelectListItem> GetStatusOptions()
        {
            List<SelectListItem> options = new List<SelectListItem>();
            options.Add(new SelectListItem
            {
                Value = "Available",
                Text = "Available"
            });
            options.Add(new SelectListItem
            {
                Value = "Out of Stock",
                Text = "Out of Stock"
            });

            return options;
        }
       
    }
}
