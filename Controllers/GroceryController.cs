﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using WebApplication.DAL;

namespace WebApplication.Controllers
{
    public class GroceryController : Controller
    {
        //DAL
        GroceryDAL GroceryContext = new GroceryDAL();
        CartDAL CartContext = new CartDAL();
        CustomerDAL CustomerContext = new CustomerDAL();

        public async Task<IActionResult> ViewGrocery(int? ID)
        {
            List<Item> GroceryList = new List<Item>();
            if (ID != null)
            {
                //Add items into cart
                Customer customer = CustomerContext.GetDetails(Convert.ToString(HttpContext.Session.GetString("LoginID")));
                CartItems cartItem = CartContext.CheckIfItemInCart(customer.ID, Convert.ToInt32(ID));
                if (cartItem != null)
                {
                    cartItem.qty += 1;
                    CartContext.UpdateCartItem(cartItem);
                }
                else
                {
                    CartItems cartData = new CartItems
                    {
                        CustomerID = customer.ID,
                        ItemID = Convert.ToInt32(ID),
                        qty = Convert.ToInt32(1)

                    };
                    CartContext.AddItemIntoCart(cartData);
                }

            }
            GroceryList = GroceryContext.GetItems();
            return View(GroceryList);
        }
    }
}
