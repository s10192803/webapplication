﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication.Models;
using System.Net.Http;
//using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
//using Newtonsoft.Json;
using WebApplication.DAL;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        //DAL
        //=======================================================================================
        CustomerDAL customerContext = new CustomerDAL();
        VolunteerDAL volunteerContext = new VolunteerDAL();
        AdminDAL adminContext = new AdminDAL();
        //=======================================================================================

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("Role") == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", HttpContext.Session.GetString("Role"));
            }
        }

        //POST: Login
        [HttpPost]
        public ActionResult Login(IFormCollection formData)
        {
            //Read inputs
            //Email to lowercase
            string email = formData["txtEmail"].ToString().ToLower();
            string password = formData["inputPassword"].ToString();
            if (volunteerContext.ValidateVolunteerEmail(email) == true || customerContext.ValidateCustomerEmail(email) == true || adminContext.ValidateAdminEmail(email) == true)
            {
                if (customerContext.AuthenticateCustomer(email, password) == true)
                {
                    // Store Login ID in session with the key "LoginID"
                    HttpContext.Session.SetString("LoginID", email);
                    Customer c = customerContext.GetDetails(email);
                    HttpContext.Session.SetInt32("ID", c.ID);
                    // Store user role "Customer" as a string in session with the key "Role"
                    HttpContext.Session.SetString("Role", "Customer");
                    HttpContext.Session.SetString("Name", c.Name);


                    return RedirectToAction("Index", "Customer");
                }
                else if (volunteerContext.AuthenticateVolunteer(email, password) == true)
                {
                    HttpContext.Session.SetString("LoginID", email);
                    HttpContext.Session.SetString("Role", "Volunteer");
                    //Store Volunteer name
                    Staff volunteer = volunteerContext.GetDetails(email);
                    HttpContext.Session.SetString("Name", volunteer.Name);
                    return RedirectToAction("Index", "Volunteer");
                }

                else if (adminContext.AuthenticateAdmin(email, password) == true)
                {
                    HttpContext.Session.SetString("LoginID", email);
                    HttpContext.Session.SetString("Role", "Admin");
                    return RedirectToAction("Index", "Admin");
                }

                else
                {
                    //Store an error message in TempData for display at the index view
                    TempData["Message"] = "Password is incorrect!";
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["Message"] = "Email address is incorrect or not registered!";
                return RedirectToAction("Index");
            }
        }
            
        public ActionResult LogOut()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }

        public ActionResult Success()
        {
            return View();
        }


        public ActionResult RoleSelection()
        {
            return View();

        }

        //Get: Forget Password
        public ActionResult ForgetPassword()
        {
            return View();
        }

        // GET: CustomerController/ForgetPass/
        public ActionResult ForgetPass()
        {
            return View();
        }

        // POST: CustomerController/ForgetPass
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgetPass(IFormCollection formData)
        {
            string email = formData["txtEmail"].ToString().ToLower();
            if (customerContext.ValidateCustomerEmail(email))
            {
                customerContext.ForgetPass(email);
            }
            else if (volunteerContext.ValidateVolunteerEmail(email))
            {
                volunteerContext.ForgetPass(email);
            }
            else
            {
                //Return error
                TempData["message"] = "There is no account related to this email";
                return View();
            }
            return View("ChangePass");
        }
        public ActionResult ForgetPassSuccess()
        {
            return View();
        }

        public IActionResult ChangePass()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public ActionResult SpaceOut()
        {
            return View();
        }

        public IActionResult Feedback()
        {
            return View();
        }

        public ActionResult AboutUs()
        {
            return View();
        }
    }
}
