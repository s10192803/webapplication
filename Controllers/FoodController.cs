﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using WebApplication.DAL;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Controllers
{
    public class FoodController : Controller
    {
        //DAL
        //=======================================================================================
        CustomerDAL CustomerContext = new CustomerDAL();
        CartDAL CartContext = new CartDAL();
        FoodDAL FoodContext = new FoodDAL();
        //=======================================================================================

        //=======================================================================================
        public IActionResult Index()
        {
            return View();
        }
        //=======================================================================================

        //=======================================================================================
        public ActionResult FoodStores()
        {
            List<Store> StoreList = FoodContext.GetAllStores();
            return View(StoreList);
        }
        //=======================================================================================

        //=======================================================================================
        public async Task<IActionResult> FoodStoreItems(int? id, int? QTY, int? ItemID)
        {
            List<Item> FoodList = new List<Item>();
            if(ItemID != null)
            {
                //Add items into cart
                Customer customer = CustomerContext.GetDetails(Convert.ToString(HttpContext.Session.GetString("LoginID")));
                CartItems cartItem = CartContext.CheckIfItemInCart(customer.ID, Convert.ToInt32(ItemID));
                if (cartItem != null)
                {
                    cartItem.qty += Convert.ToInt32(QTY);
                    CartContext.UpdateCartItem(cartItem);
                }
                else
                {
                    CartItems cartData = new CartItems
                    {
                        CustomerID = customer.ID,
                        ItemID = Convert.ToInt32(ItemID),
                        qty = Convert.ToInt32(QTY)

                    };
                    CartContext.AddItemIntoCart(cartData);
                }
                
            }
            string StoreName = FoodContext.GetStore(Convert.ToInt32(id)).StoreName;
            FoodList = FoodContext.GetFoodItems(StoreName);
            TempData["StoreName"] = StoreName;
            return View(FoodList);
        }
        //=======================================================================================
    }
}
