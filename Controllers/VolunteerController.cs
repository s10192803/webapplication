﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using WebApplication.DAL;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;


namespace WebApplication.Controllers
{
    public class VolunteerController : Controller
    {
        //DAL
        //=======================================================================================
        VolunteerDAL VolunteerContext = new VolunteerDAL();
        CustomerDAL CustomerContext = new CustomerDAL();
        FoodDAL FoodContext = new FoodDAL();
        GroceryDAL GroceryContext = new GroceryDAL();
        OrderDAL OrderContext = new OrderDAL();
        //=======================================================================================

        // GET: StaffController
        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("Role") == "Volunteer")
            {
                return View();
            }
            else if (HttpContext.Session.GetString("Role") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", HttpContext.Session.GetString("Role"));
            }

        }


        // GET: Volunteer/Details/5
        public ActionResult ViewProfile()
        {
            if (HttpContext.Session.GetString("Role") == "Volunteer")
            {
                Staff volunteer = new Staff();
                volunteer = VolunteerContext.GetDetails(HttpContext.Session.GetString("LoginID"));
                return View(volunteer);

            }
            else if (HttpContext.Session.GetString("Role") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", HttpContext.Session.GetString("Role"));
            }

        }

        // GET: StaffController/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Staff volunteer)
        {
            if (ModelState.IsValid)
            {
                volunteer.ID = VolunteerContext.Add(volunteer);
                TempData["Message"] = "Your account has been created successfully, " + volunteer.Name + ".";

                return RedirectToAction("Success", "Home");
           }

            return View(volunteer);

        }

        // POST: StaffController/Create


        // GET: StaffController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: StaffController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
      

        public ActionResult VolunteerForgetPass()
        {
            return View();
        }

        [HttpPost]
        public ActionResult VolunteerForgetPass(IFormCollection formData)
        {
            string email = formData["txtEmail"].ToString().ToLower();
            if (VolunteerContext.ValidateVolunteerEmail(email))
            {
                VolunteerContext.ForgetPass(email);
                return RedirectToAction("ForgetPassSuccess", "Home");
            }
            else
            {
                //Return error
                TempData["message"] = "There is no account related to this email";
                return View();
            }
        }

        // GET: VolunteerController/Edit
        public ActionResult Edit()
        {
            Staff volunteer;
            if (HttpContext.Session.GetString("Role") == "Volunteer")
            {
                volunteer = VolunteerContext.GetDetails(Convert.ToString(HttpContext.Session.GetString("LoginID")));
                return View(volunteer);
            }
            else if (HttpContext.Session.GetString("Role") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", HttpContext.Session.GetString("Role"));
            }

        }
        // POST: VolunteerController/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Staff volunteer)
        {
            volunteer.ID = VolunteerContext.Edit(volunteer);
            HttpContext.Session.SetString("Name", volunteer.Name);

            return RedirectToAction("ViewProfile", "Volunteer");
        }

        public ActionResult AvailableJobs()
        {
            List<Order> OrderList = OrderContext.GetAllAvailJob();
            _ViewAllJobs data = new _ViewAllJobs();
            data.CustomerList = new List<Customer>();
            data.ItemList = new List<Item>();
            data.OrderItemList = new List<CartItems>();
            data.OrderList = OrderList;
            //Get Relevant customer
            foreach (var i in data.OrderList)
            {
                data.CustomerList.Add(CustomerContext.GetCustomer(i.CustomerID));
                data.OrderItemList.AddRange(OrderContext.GetAllOrderItems(i.OrderID));
            }

            if (data.OrderItemList != null)
            {
                foreach (var i in data.OrderItemList)
                {
                    data.ItemList.Add(OrderContext.GetItem(i.ItemID));
                }
            }
            
            return View(data);

        }

        public ActionResult ViewAcceptedJobs()
        {
            Staff volunteer = VolunteerContext.GetDetails(HttpContext.Session.GetString("LoginID"));
            _ViewAllJobs data = new _ViewAllJobs();
            data.CustomerList = new List<Customer>();
            data.ItemList = new List<Item>();
            data.OrderItemList = new List<CartItems>();
            data.OrderList = OrderContext.GetAcceptedJobs(volunteer.ID);
            //Get Relevant customer
            foreach (var i in data.OrderList)
            {
                data.CustomerList.Add(CustomerContext.GetCustomer(i.CustomerID));
                data.OrderItemList.AddRange(OrderContext.GetAllOrderItems(i.OrderID));           
            }
            if(data.OrderItemList != null)
            {
                foreach (var i in data.OrderItemList)
                {
                    data.ItemList.Add(OrderContext.GetItem(i.ItemID));
                }
            }
            return View(data);
        }

        public ActionResult CompleteJob(int id)
        {
            OrderContext.CompleteOrder(id);
            return RedirectToAction("Index", "Volunteer");
        }

        public ActionResult AcceptJob(int id)
        {
            Staff volunteer = VolunteerContext.GetDetails(HttpContext.Session.GetString("LoginID"));
            OrderContext.AcceptJob(id, volunteer.ID);
            return RedirectToAction("Index", "Volunteer");
        }
    }
}
