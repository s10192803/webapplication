﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using WebApplication.DAL;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Controllers
{
    public class CartController : Controller
    {
        //DAL
        //=======================================================================================
        CustomerDAL CustomerContext = new CustomerDAL();
        CartDAL CartContext = new CartDAL();
        FoodDAL FoodContext = new FoodDAL();
        //=======================================================================================

        public IActionResult ViewCart()
        {
            Customer customer = CustomerContext.GetDetails(Convert.ToString(HttpContext.Session.GetString("LoginID")));
            _ViewCartItem data = new _ViewCartItem();
            List<CartItems> CartItemList = CartContext.GetAllCartItem(customer.ID);
            List<Item> ItemList = new List<Item>();
            foreach(var i in CartItemList)
            {
                Item temp = CartContext.GetItem(i.ItemID);
                ItemList.Add(temp);
                data.TotalPrice += (temp.Price*i.qty);
            }

            data.CartItemList = CartItemList;
            data.ItemList = ItemList;
            return View(data);
        }

        public IActionResult DeleteCartItem(int id)
        {
            Customer customer = CustomerContext.GetDetails(Convert.ToString(HttpContext.Session.GetString("LoginID")));
            CartContext.DeleteCartitem(id, customer.ID);

            return RedirectToAction("ViewCart", "Cart");
        }
    }
}
