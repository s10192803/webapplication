﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using WebApplication.DAL;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Controllers
{
    public class OrderController : Controller
    {
        //=======================================================================================
        CustomerDAL CustomerContext = new CustomerDAL();
        CartDAL CartContext = new CartDAL();
        OrderDAL OrderContext = new OrderDAL();
        //=======================================================================================

        public ActionResult CheckOut()
        {
            Customer customer = CustomerContext.GetDetails(Convert.ToString(HttpContext.Session.GetString("LoginID")));
            decimal TotalPrice = 0;
            List<CartItems> CartItemList = CartContext.GetAllCartItem(customer.ID);
            List<Item> ItemList = new List<Item>();
            foreach (var i in CartItemList)
            {
                Item item = CartContext.GetItem(i.ItemID);
                TotalPrice += item.Price * i.qty;
                ItemList.Add(item);
            }
            _ViewCheckOut data = new _ViewCheckOut();
            data.CartList = CartItemList;
            data.ItemList = ItemList;
            data.TotalPrice = TotalPrice;
            if (data.CartList.Count == 0)
            {
                TempData["CheckOutError"] = "The cart is empty, please add items before checking out!";
                return RedirectToAction("ViewCart", "Cart");
            }
            bool success = OrderContext.GetOrderStatus((int)HttpContext.Session.GetInt32("ID"));
            if (success != true)
            {
                TempData["CheckOutError"] = "You have already placed an order, pleased wait for your order to be completed before placing another one.";
                return RedirectToAction("ViewCart", "Cart");
            }
            return View(data);
        }

        public ActionResult PlaceOrder()
        {
            Customer customer = CustomerContext.GetDetails(Convert.ToString(HttpContext.Session.GetString("LoginID")));
            Order OrderData = new Order();
            OrderData.CustomerID = customer.ID;
            OrderData.Status = "Processing";
            OrderData.OrderCreated = DateTime.Now;
            OrderData.TotalCost = OrderContext.GetTotalCost(customer.ID);

            int OrderID = OrderContext.CreateOrder(OrderData);

            //Create order item
            List<CartItems> CartItemList = CartContext.GetAllCartItem(customer.ID);
            List<Item> ItemList = new List<Item>();
            foreach (var i in CartItemList)
            {
                i.CustomerID = OrderID;
                OrderContext.AddOrderItem(i);
            }

            CartContext.EmptyCart(customer.ID);
            return RedirectToAction("Success", "Order");
        }

        public ActionResult Success()
        {
            return View();
        }
    }
}
