﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;
using WebApplication.DAL;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Controllers
{
    public class CustomerController : Controller
    {
        //DAL
        //=======================================================================================
        CustomerDAL CustomerContext = new CustomerDAL();
        CartDAL CartContext = new CartDAL();
        GroceryDAL GroceryContext = new GroceryDAL();
        FoodDAL FoodContext = new FoodDAL();
        OrderDAL OrderContext = new OrderDAL();

        //=======================================================================================

        // GET: CustomerController
        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("Role") == "Customer")
            {
                return View();
            }
            else if (HttpContext.Session.GetString("Role") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", HttpContext.Session.GetString("Role"));
            }
        }

       

        //GET: CustomerController/Details/5
        public ActionResult ViewProfile()
        {
            //To check if the session has ID
            if (HttpContext.Session.GetString("Role") == "Customer")
            {
                Customer customer = new Customer();
                customer = CustomerContext.GetDetails(HttpContext.Session.GetString("LoginID"));
                return View(customer);
            }
            else if (HttpContext.Session.GetString("Role") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", HttpContext.Session.GetString("Role"));
            }

        }

        
        // GET: CustomerController/

        public ActionResult Edit()
        {
            Customer customer;
            if (HttpContext.Session.GetString("Role") == "Customer")
            {
                customer = CustomerContext.GetDetails(HttpContext.Session.GetString("LoginID"));
                return View(customer);

            }

            else if (HttpContext.Session.GetString("Role") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", HttpContext.Session.GetString("Role"));
            }

        }
        // POST: CustomerController/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Customer customer)
        {
            customer.ID = CustomerContext.Edit(customer);
            HttpContext.Session.SetString("Name", customer.Name);
            return RedirectToAction("ViewProfile", "Customer");


        }
        

        public ActionResult CustomerForgetPass()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CustomerForgetPass(IFormCollection formData)
        {
            string email = formData["txtEmail"].ToString().ToLower();
            if (CustomerContext.ValidateCustomerEmail(email))
            {
                CustomerContext.ForgetPass(email);
                return RedirectToAction("ForgetPassSuccess", "Home");
            }
            else
            {
                //Return error
                TempData["message"] = "There is no account related to this email";
                return View();
            }
        }
        // GET: CustomerController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CustomerController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Customer customer)
        {
            if (ModelState.IsValid)
            {
                customer.ID = CustomerContext.Add(customer);
                TempData["Message"] = "Your account has been created successfully, " + customer.Name + ".";
                return RedirectToAction("Success", "Home");

            }
            
            return View(customer);
        }
        //=======================================================================================
        
        public ActionResult AddItem()
        {
            return View();
        }
        public ActionResult OrderHistory()
        {
            Customer customer = CustomerContext.GetDetails(Convert.ToString(HttpContext.Session.GetString("LoginID")));
            List<_ViewOrderHistory> data = OrderContext.GetCustomerOrder(customer.ID);
            foreach(var i in data)
            {
                i.ItemList = OrderContext.GetAllItem(i.OrderID);
                i.OrderItem = OrderContext.GetAllOrderItems(i.OrderID);
                i.TotalPrice = OrderContext.GetTotalPrice(i.OrderID);
            }

            //Chart
            List<Order> OrderHistory = OrderContext.GetOrderHistory(customer.ID);
            //Adding month into a string list 
            List<string> Months = new List<string>();
            Months.Add("Jan");
            Months.Add("Feb");
            Months.Add("March");
            Months.Add("April");
            Months.Add("May");
            Months.Add("Jun");
            Months.Add("July");
            Months.Add("Aug");
            Months.Add("Sep");
            Months.Add("Oct");
            Months.Add("Nov");
            Months.Add("Dec");
            List<int> Frequency = new List<int>();
            for (int i = 0; i <= 11; i++)
            {
                Frequency.Add(0);
            }
            foreach (var i in OrderHistory)
            {
                if(i.OrderCreated.Year == DateTime.Now.Year)
                {
                    Frequency[i.OrderCreated.Month - 1] += 1;
                }
            }
            ViewBag.Frequency = Frequency.ToList();
            ViewBag.Months = Months;

            return View(data);
        }
        public ActionResult OrderStatus()
        {
            Customer customer = CustomerContext.GetDetails(Convert.ToString(HttpContext.Session.GetString("LoginID")));
            List<_ViewOrderStatus> MasterModel = OrderContext.GetCurrentOrder(customer.ID);
            foreach(var i in MasterModel)
            {
                i.OrderItem = OrderContext.GetAllOrderItems(i.OrderID);
                i.ItemList = OrderContext.GetAllItem(i.OrderID);
            }
            
            return View(MasterModel);
        }
    }
}