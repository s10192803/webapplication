﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebApplication.Models;

namespace WebApplication.DAL
{
    public class CustomerDAL
    {
        //=======================================================================================
        //Constructor
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public CustomerDAL()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("B_United_SG_ConnectionString");
            conn = new SqlConnection(strConn);
        }
        //=======================================================================================

        //=======================================================================================
        public int Add(Customer customer)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO Elderly (Name, EmailAddr, DOB, PhoneNum, Address, Dietary, MedHist, Gender, Password)
                                OUTPUT INSERTED.CustomerID
                                VALUES(@name, @email, @dob, @phonenum, @address, @Dietary, @medhist, @gender, @password)";
            cmd.Parameters.AddWithValue("@name", customer.Name);
            cmd.Parameters.AddWithValue("@email", customer.EmailAddr);
            cmd.Parameters.AddWithValue("@dob", customer.DOB);
            cmd.Parameters.AddWithValue("@phonenum", customer.PhoneNum);
            cmd.Parameters.AddWithValue("@address", customer.Address);
            if (customer.Dietary == null)
            {
                cmd.Parameters.AddWithValue("@Dietary", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Dietary", customer.Dietary);

            }
            if (customer.MedicalHistory == null)
            {
                cmd.Parameters.AddWithValue("@medhist", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@medhist", customer.MedicalHistory);
            }
            //if (customer.eWallet == null)
            //{
            //    cmd.Parameters.AddWithValue("@balance", DBNull.Value);
            //}
            cmd.Parameters.AddWithValue("@gender", customer.Gender);
            cmd.Parameters.AddWithValue("@password", customer.Password);
            conn.Open();
            customer.ID = (int)cmd.ExecuteScalar();
            conn.Close();
            return customer.ID;
        }

        //=======================================================================================
        //Get all customer
        //=======================================================================================
        public List<Customer> GetAllCustomer()
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Elderly ORDER BY CustomerID";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<Customer> CustomerList = new List<Customer>();
            while (reader.Read())
            {
                CustomerList.Add(
                new Customer
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1),
                    EmailAddr = reader.GetString(2),
                    DOB = reader.GetDateTime(3),
                    PhoneNum = reader.GetString(4),
                    Address = reader.GetString(5),
                    Dietary = !reader.IsDBNull(6) ?
                                reader.GetString(6) : (string?)null,
                    MedicalHistory = !reader.IsDBNull(7) ?
                                reader.GetString(7) : (string?)null,
                    Gender = reader.GetString(9),
                    Password = reader.GetString(10) 
                }
                );
            }
            reader.Close();
            conn.Close();

            return CustomerList;
        }

        public Customer GetCustomer(int custID)
        {
            SqlCommand cmd = conn.CreateCommand();
            Customer customer = new Customer();
            cmd.CommandText = @"SELECT * FROM Elderly WHERE CustomerID = @custID";

            cmd.Parameters.AddWithValue("@custID", custID);

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                customer.ID = reader.GetInt32(0);
                customer.Name = reader.GetString(1);
                customer.EmailAddr = reader.GetString(2);
                customer.DOB = reader.GetDateTime(3);
                customer.PhoneNum = reader.GetString(4);
                customer.Address = reader.GetString(5);
                customer.Dietary = !reader.IsDBNull(6) ?
                            reader.GetString(6) : (string?)null;
                customer.MedicalHistory = !reader.IsDBNull(7) ?
                            reader.GetString(7) : (string?)null;
                customer.Gender = reader.GetString(9);
                customer.Password = reader.GetString(10);
                
            }
            reader.Close();
            conn.Close();
            return customer;
        }
        //=======================================================================================
        

        //=======================================================================================
        public bool AuthenticateCustomer(string email, string password)
        {
            bool authenticate = false;

            //Create a SqlCommand object and specify the SQL command
            //to get customer record to validate password
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT Password FROM Elderly
                                WHERE EmailAddr=@emailEntered";
            cmd.Parameters.AddWithValue("@emailEntered", email);

            //open DB connection and execute SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows) //records found
            {
                while (reader.Read())
                {
                    if (reader.GetString(0) == password) //passwords match
                    {
                        authenticate = true;
                    }
                }
            }

            else
            {
                //no record found
            }
            reader.Close();
            conn.Close();

            return authenticate;

        }
        //=======================================================================================
        public bool ValidateCustomerEmail(string email)
        {
            bool exist = false;

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT EmailAddr FROM Elderly";

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if ((reader.GetString(0)).ToLower() == email.ToLower())
                    {
                        //There is an existing email
                        exist = true;
                        break;
                    }
                }
            }

            else
            {
                //no record found
                exist = false;
            }
            reader.Close();
            conn.Close();

            return exist;
        }
        //======================================================================================

        public bool ValidateCustomerPhoneNum(string hpno)
        {
            bool exist = false;

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT PhoneNum FROM Elderly";

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if ((reader.GetString(0)) == hpno)
                    {
                        //There is an existing Hp Num
                        exist = true;
                        break;
                    }
                }
            }

            else
            {
                //no record found
                exist = false;
            }
            reader.Close();
            conn.Close();

            return exist;
        }

        //=======================================================================================
        public Customer GetDetails(string Email)
        {
            Customer customer = new Customer();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Elderly
                                WHERE EmailAddr = @SelectedCustomerEmail";
            cmd.Parameters.AddWithValue("@SelectedCustomerEmail", Email);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    customer.ID = reader.GetInt32(0);
                    customer.Name = reader.GetString(1);
                    customer.EmailAddr = reader.GetString(2);
                    customer.DOB = reader.GetDateTime(3);
                    customer.PhoneNum = reader.GetString(4);
                    customer.Address = reader.GetString(5);
                    customer.Dietary = !reader.IsDBNull(6) ?
                                reader.GetString(6) : (string?)null;
                    customer.MedicalHistory = !reader.IsDBNull(7) ?
                                reader.GetString(7) : (string?)null;
                    customer.Gender = reader.GetString(9);
                    customer.Password = reader.GetString(10);
                }
            }
            reader.Close();
            conn.Close();
            return customer;
        }
       

        //=======================================================================================
        public void ForgetPass(string email)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Elderly SET Password=@Password 
                                WHERE EmailAddr=@email";
            cmd.Parameters.AddWithValue("@Password", "password");
            cmd.Parameters.AddWithValue("@email", email);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        //=======================================================================================
         public int Edit(Customer customer)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Elderly 
                                SET Name=@Name, DOB=@DOB, PhoneNum=@PhoneNum,
                                Address=@Address, MedHist=@MedicalHist, Dietary=@Dietary, Password=@Pass
                                WHERE EmailAddr=@Email";
            cmd.Parameters.AddWithValue("@Name", customer.Name);
            cmd.Parameters.AddWithValue("@Email", customer.EmailAddr);
            cmd.Parameters.AddWithValue("@DOB", customer.DOB);
            cmd.Parameters.AddWithValue("@PhoneNum", customer.PhoneNum);
            cmd.Parameters.AddWithValue("@Address", customer.Address);
            cmd.Parameters.AddWithValue("@Pass", customer.Password);
            if (customer.Dietary == null)
            {
                cmd.Parameters.AddWithValue("@Dietary", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Dietary", customer.Dietary);

            }
            if (customer.MedicalHistory == null)
            {
                cmd.Parameters.AddWithValue("@MedicalHist", "");
            }
            else
            {
                cmd.Parameters.AddWithValue("@MedicalHist", customer.MedicalHistory);
            }
        
            conn.Open();
            int count = (int)cmd.ExecuteNonQuery();
            conn.Close();
            return count;
        }
    }
}