﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebApplication.Models;


namespace WebApplication.DAL
{
    public class OrderDAL
    {
        //=======================================================================================
        //Constructor
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public OrderDAL()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("B_United_SG_ConnectionString");
            conn = new SqlConnection(strConn);
        }
        //=======================================================================================
        public decimal GetTotalCost(int CustomerID)
        {
            SqlCommand cmd = conn.CreateCommand();
            decimal TotalCost = 0;
            cmd.CommandText = @"SELECT Sum(Price*Quantity) FROM Item INNER JOIN 
                                CartItem on Item.ItemId = CartItem.ItemID 
                                WHERE CustomerID = @CustomerID";
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    TotalCost = reader.GetDecimal(0);
                }
            }
            reader.Close();
            conn.Close();
            return TotalCost;
        }
        public int CreateOrder(Order data)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO Orders(CustomerID, Status, OrderCreated, TotalCost)
                                OUTPUT INSERTED.OrderID
                                VALUES(@CustomerID, @Status, @OrderCreated, @TotalCost)";
            cmd.Parameters.AddWithValue("@CustomerID", data.CustomerID);
            cmd.Parameters.AddWithValue("@Status", data.Status);
            cmd.Parameters.AddWithValue("@OrderCreated", data.OrderCreated);
            cmd.Parameters.AddWithValue("@TotalCost", data.TotalCost);
            conn.Open();
            int OrderID = (int)cmd.ExecuteScalar();
            conn.Close();
            return OrderID;
        }
        public void AddOrderItem(CartItems data)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO OrderItem(OrderID, ItemID, Quantity)
                                OUTPUT INSERTED.OrderID
                                VALUES(@OrderID, @ItemID, @Quantity)";
            cmd.Parameters.AddWithValue("@OrderID", data.CustomerID);
            cmd.Parameters.AddWithValue("@ItemID", data.ItemID);
            cmd.Parameters.AddWithValue("@Quantity", data.qty);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
        }
        public List<Order> GetAllAvailJob()
        {
            List<Order> OrderList = new List<Order>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Orders WHERE Status = 'Processing'";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                OrderList.Add(
                    new Order
                    {
                        OrderID = reader.GetInt32(0),
                        CustomerID = reader.GetInt32(1),
                        VolunteerID = !reader.IsDBNull(2) ?
                                reader.GetInt32(2) : (int?)null,
                        Status = reader.GetString(3),
                        Remarks = !reader.IsDBNull(4) ?
                                reader.GetString(4) : (string?)null,
                        DelieveryDateTime = !reader.IsDBNull(5) ?
                                reader.GetDateTime(5) : (DateTime?)null,
                        OrderCreated = reader.GetDateTime(6),
                        TotalCost = reader.GetDecimal(7),
                    }
                    ) ;
            }
            reader.Close();
            conn.Close();
            return OrderList;
        }

        public List<CartItems> GetAllOrderItems(int OrderID)
        {
            List<CartItems> OrderItemList = new List<CartItems>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM OrderItem WHERE OrderID = @OrderID";
            cmd.Parameters.AddWithValue("@OrderID", OrderID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                OrderItemList.Add(
                    new CartItems
                    {
                        CustomerID = reader.GetInt32(0),
                        ItemID = reader.GetInt32(1),
                        qty = reader.GetInt32(2)
                    }
                    ) ;
            }
            reader.Close();
            conn.Close();
            return OrderItemList;
        }

        public Item GetItem(int ItemID)
        {
            Item item = new Item();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Item Where ItemId = @ItemID";
            cmd.Parameters.AddWithValue("@ItemID", ItemID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    item.ItemID = reader.GetInt32(0);
                    item.Name = reader.GetString(1);
                    item.Status = reader.GetString(2);
                    item.Price = reader.GetDecimal(3);
                    item.Category = reader.GetString(4);
                    item.Dietary = !reader.IsDBNull(5) ?
                                reader.GetString(5) : (string?)null;
                    item.StoreName = !reader.IsDBNull(6) ?
                                reader.GetString(6) : (string?)null;
                }
            }
            reader.Close();
            conn.Close();
            return item;
        }

        public List<Order> GetAcceptedJobs(int VolunteerID)
        {
            List<Order> OrderList = new List<Order>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Orders WHERE VolunteerID = @VolID AND Status = 'In Progress'";
            cmd.Parameters.AddWithValue("@VolID", VolunteerID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                OrderList.Add(
                    new Order
                    {
                        OrderID = reader.GetInt32(0),
                        CustomerID = reader.GetInt32(1),
                        VolunteerID = !reader.IsDBNull(2) ?
                                reader.GetInt32(2) : (int?)null,
                        Status = reader.GetString(3),
                        Remarks = !reader.IsDBNull(4) ?
                                reader.GetString(4) : (string?)null,
                        DelieveryDateTime = !reader.IsDBNull(5) ?
                                reader.GetDateTime(5) : (DateTime?)null,
                        OrderCreated = reader.GetDateTime(6),
                        TotalCost = reader.GetDecimal(7),
                    }
                    );
            }
            reader.Close();
            conn.Close();
            return OrderList;
        }

        public void CompleteOrder(int OrderID)
        {
            Order order = new Order();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Orders 
                               SET [Status] = 'Completed', DeliveryDateTime = @DeliveryDate WHERE OrderID = @OrderID";
            cmd.Parameters.AddWithValue("@OrderID", OrderID);
            cmd.Parameters.AddWithValue("@DeliveryDate", DateTime.Now);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void AcceptJob(int OrderID, int VolunteerID)
        {
            Order order = new Order();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Orders 
                               SET [Status] = 'In Progress', VolunteerID = @VolunteerID
                               WHERE OrderID = @OrderID";
            cmd.Parameters.AddWithValue("@OrderID", OrderID);
            cmd.Parameters.AddWithValue("@VolunteerID", VolunteerID);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public bool GetOrderStatus(int ID)
        {
            string status = "";
            bool success = true;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"Select [Status] from Orders 
                               WHERE CustomerID = @custID";
            cmd.Parameters.AddWithValue("@custID", ID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                status = reader.GetString(0);
                if (status != "Completed")
                {
                    success = false;
                    break;
                }
            }
            conn.Close();
            return success;
        }
        public List<_ViewOrderHistory> GetCustomerOrder(int CustomerID)
        {
            List<_ViewOrderHistory> DataList = new List<_ViewOrderHistory>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT OrderID, Status, OrderCreated FROM Orders WHERE CustomerID = @custID AND Status = 'Completed'";
            cmd.Parameters.AddWithValue("@custID", CustomerID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataList.Add(
                    new _ViewOrderHistory
                    {
                        OrderID = reader.GetInt32(0),
                        Status = reader.GetString(1),
                        OrderCreated = reader.GetDateTime(2)
                    }
                    );
            }
            conn.Close();
            return DataList;
        }

        public List<Item> GetAllItem(int OrderID)
        {
            List<Item> DataList = new List<Item>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT Item.ItemId, Item.ItemName FROM Item 
                                INNER JOIN OrderItem ON OrderItem.ItemID = Item.ItemId
                                WHERE OrderItem.OrderID = @OrderID";
            cmd.Parameters.AddWithValue("@OrderID", OrderID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataList.Add(
                    new Item
                    {
                        ItemID = reader.GetInt32(0),
                        Name = reader.GetString(1),
                    }
                    );
            }
            conn.Close();
            return DataList;
        }
        public decimal GetTotalPrice(int OrderID)
        {
            decimal TotalPrice = 0;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT SUM(Price* Quantity) FROM Item 
                                INNER JOIN OrderItem ON OrderItem.ItemID = Item.ItemId
                                WHERE OrderItem.OrderID = @OrderID";
            cmd.Parameters.AddWithValue("@OrderID", OrderID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                TotalPrice = reader.GetDecimal(0);
            }
            conn.Close();
            return TotalPrice;
        }
        public List<Order> GetOrderHistory(int CustomerID)
        {
            List<Order> DataList = new List<Order>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Orders WHERE CustomerID = @CustID AND [Status] = 'Completed'
                                        ORDER BY OrderID";
            cmd.Parameters.AddWithValue("@CustID", CustomerID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataList.Add(
                    new Order
                    {
                        OrderID = reader.GetInt32(0),
                        CustomerID = reader.GetInt32(1),
                        VolunteerID = !reader.IsDBNull(2) ?
                                reader.GetInt32(2) : (int?)null,
                        Status = reader.GetString(3),
                        Remarks = !reader.IsDBNull(4) ?
                                reader.GetString(4) : (string?)null,
                        DelieveryDateTime = !reader.IsDBNull(5) ?
                                reader.GetDateTime(5) : (DateTime?)null,
                        OrderCreated = reader.GetDateTime(6),
                        TotalCost = reader.GetDecimal(7),
                    }
                    );
            }
            conn.Close();
            return DataList;
        }

        public List<_ViewOrderStatus> GetCurrentOrder(int CustomerID)
        {
            List<_ViewOrderStatus> DataList = new List<_ViewOrderStatus>();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT OrderID, [Status], OrderCreated, TotalCost FROM Orders WHERE CustomerID = @custID AND Status != 'Completed'";
            cmd.Parameters.AddWithValue("@custID", CustomerID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataList.Add(
                    new _ViewOrderStatus
                    {
                        OrderID = reader.GetInt32(0),
                        Status = reader.GetString(1),
                        DeliveryDateTime = !reader.IsDBNull(2) ?
                                reader.GetDateTime(2) : (DateTime?)null,
                        TotalPrice = reader.GetDecimal(3)
                    }
                    );
            }
            conn.Close();
            return DataList;
        }
        //public List<CartItems> GetCurrentOrderItem(int OrderID)
        //{
        //    CartItems Item = new CartItems();
        //    SqlCommand cmd = conn.CreateCommand();
        //    cmd.CommandText = @"SELECT * FROM OrderItem WHERE OrderID = @OrderID";
        //    cmd.Parameters.AddWithValue("@OrderID", OrderID);
        //    conn.Open();
        //    SqlDataReader reader = cmd.ExecuteReader();
        //    if (reader.HasRows)
        //    {
        //        while (reader.Read())
        //        {
        //            Item.ItemID = reader.GetInt32(0);
        //            Item.qty = reader.GetInt32(2);
        //        }
        //    }
        //    reader.Close();
        //    conn.Close();
        //    return Item;
        //}


    }
}
