﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebApplication.Models;

namespace WebApplication.DAL
{
    public class CartDAL
    {
        //=======================================================================================
        //Constructor
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public CartDAL()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("B_United_SG_ConnectionString");
            conn = new SqlConnection(strConn);
        }
        //=======================================================================================
        public List<CartItems> GetAllCartItem(int CustomerID)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CartItem WHERE CustomerID = @customerID";
            cmd.Parameters.AddWithValue("@customerID", CustomerID);
            conn.Open();
            List<CartItems> CartItemList = new List<CartItems>();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                CartItemList.Add(
                    new CartItems
                    {
                        CustomerID = reader.GetInt32(0),
                        ItemID = reader.GetInt32(1),
                        qty = reader.GetInt32(2),
                    }
                    );
            }
            reader.Close();
            conn.Close();
            return CartItemList;
        }
        public Item GetItem(int ItemID)
        {
            Item item = new Item();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Item WHERE ItemId = @ItemID";
            cmd.Parameters.AddWithValue("@ItemID", ItemID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    item.ItemID = reader.GetInt32(0);
                    item.Name = reader.GetString(1);
                    item.Status = reader.GetString(2);
                    item.Price = reader.GetDecimal(3);
                    item.Category = reader.GetString(4);
                    item.Dietary = !reader.IsDBNull(5) ?
                                    reader.GetString(5): (string?)null;
                    item.StoreName = !reader.IsDBNull(6) ?
                                    reader.GetString(6) : (string?)null;
                }
            }
            reader.Close();
            conn.Close();
            return item;
        }
        public void AddItemIntoCart(CartItems data)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO CartItem(CustomerID, ItemID, Quantity)
                                VALUES(@customerID, @ItemID, @QTY)";
            cmd.Parameters.AddWithValue("@customerID", data.CustomerID);
            cmd.Parameters.AddWithValue("@ItemID", data.ItemID);
            cmd.Parameters.AddWithValue("@QTY", data.qty);
            conn.Open();
            cmd.ExecuteScalar();
            conn.Close();
        }
        public CartItems CheckIfItemInCart(int CustomerID, int ItemID)
        {
            CartItems item = new CartItems();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CartItem WHERE CustomerID = @CustomerID AND ItemID = @ItemID";
            cmd.Parameters.AddWithValue(@"CustomerID", CustomerID);
            cmd.Parameters.AddWithValue(@"ItemID", ItemID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    item.CustomerID = reader.GetInt32(0);
                    item.ItemID = reader.GetInt32(1);
                    item.qty = reader.GetInt32(2);
                }
            }
            else
            {
                item = null;
            }
            reader.Close();
            conn.Close();
            return item;
        }
        public void UpdateCartItem(CartItems data)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE CartItem
                                SET Quantity = @QTY
                                WHERE (CustomerID = @CustomerID) AND (ItemID = @ItemID)";
            cmd.Parameters.AddWithValue("@CustomerID", data.CustomerID);
            cmd.Parameters.AddWithValue("@ItemID", data.ItemID);
            cmd.Parameters.AddWithValue("@QTY", data.qty);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

        }
        public void DeleteCartitem(int itemid, int custid)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM CartItem WHERE CustomerID = @custid AND ItemID = @itemid";
            cmd.Parameters.AddWithValue(@"custid", custid);
            cmd.Parameters.AddWithValue("@itemid", itemid);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        public void EmptyCart(int id)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM CartItem WHERE CustomerID = @id";
            cmd.Parameters.AddWithValue(@"id", id);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}
