﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebApplication.Models;

namespace WebApplication.DAL
{
    public class AdminDAL
    {
        //=======================================================================================
        //Constructor
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public AdminDAL()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("B_United_SG_ConnectionString");
            conn = new SqlConnection(strConn);
        }
        //=======================================================================================
        public bool AuthenticateAdmin(string email, string password)
        {

            bool authenticate = false;
            //Create a SqlCommand object and specify the SQL command
            //to get customer record to validate password
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT Password FROM Admin
                                WHERE EmailAddr=@emailEntered";
            cmd.Parameters.AddWithValue("@emailEntered", email);

            //open DB connection and execute SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows) //records found
            {
                while (reader.Read())
                {
                    if (reader.GetString(0) == password) //passwords match
                    {
                        authenticate = true;
                    }
                }
            }

            else
            {
                //no record found (no email)
            }
            reader.Close();
            conn.Close();

            return authenticate;
        }

        public bool ValidateAdminEmail(string email)
        {
            bool exist = false;

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT EmailAddr FROM Admin";

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if ((reader.GetString(0)).ToLower() == email.ToLower())
                    {
                        //There is an existing email
                        exist = true;
                        break;
                    }
                }
            }

            else
            {
                //no record found
                exist = false;
            }
            reader.Close();
            conn.Close();

            return exist;
        }

        public Customer GetCustomerDetails(int id)
        {
            Customer customer = new Customer();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Elderly
                                WHERE CustomerID = @CustomerID";
            cmd.Parameters.AddWithValue("@CustomerID", id);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    customer.ID = reader.GetInt32(0);
                    customer.Name = reader.GetString(1);
                    customer.EmailAddr = reader.GetString(2);
                    customer.DOB = reader.GetDateTime(3);
                    customer.PhoneNum = reader.GetString(4);
                    customer.Address = reader.GetString(5);
                    customer.Dietary = !reader.IsDBNull(6) ?
                                reader.GetString(6) : (string?)null;
                    customer.MedicalHistory = !reader.IsDBNull(7) ?
                                reader.GetString(7) : (string?)null;
                    customer.Gender = reader.GetString(9);
                    customer.Password = reader.GetString(10);
                }
            }
            reader.Close();
            conn.Close();
            return customer;
        }
        public Staff GetVolunteerDetails(int id)
        {
            Staff volunteer = new Staff();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Volunteer
                                WHERE VolunteerID = @VolunteerID";
            cmd.Parameters.AddWithValue("@VolunteerID", id);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    volunteer.ID = reader.GetInt32(0);
                    volunteer.Name = reader.GetString(1);
                    volunteer.EmailAddr = reader.GetString(2);
                    volunteer.DOB = reader.GetDateTime(3);
                    volunteer.PhoneNum = reader.GetString(4);
                    volunteer.Gender = reader.GetString(5);
                    volunteer.Password = reader.GetString(6);
                    volunteer.Role = "Volunteer";
                }
            }
            reader.Close();
            conn.Close();
            return volunteer;
        }

        public int AddGrocery(Item item)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO Item(ItemName,Status, Price, Category, Dietary)
                                OUTPUT INSERTED.ItemId
                               VALUES(@name, @status, @price, @category, @dietary)";

            cmd.Parameters.AddWithValue("@name", item.Name);
            cmd.Parameters.AddWithValue("@status", item.Status);
            cmd.Parameters.AddWithValue("@price", item.Price);
            cmd.Parameters.AddWithValue("@category", item.Category);
            if (item.Dietary == null)
            {
                cmd.Parameters.AddWithValue("@dietary", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@dietary", item.Dietary);

            }

            conn.Open();
            item.ItemID = (int)cmd.ExecuteScalar();
            conn.Close();
            return item.ItemID;

        }
        public int AddStore(Store store)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO Store(StoreName,Area, Address)
                                OUTPUT INSERTED.StoreID
                               VALUES(@name, @area, @address)";

            cmd.Parameters.AddWithValue("@name", store.StoreName);
            cmd.Parameters.AddWithValue("@area", store.Area);
            cmd.Parameters.AddWithValue("@address", store.Address);

            conn.Open();
            store.ID = (int)cmd.ExecuteScalar();
            conn.Close();
            return store.ID;

        }

        public List<Store> GetStores()
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Store ORDER BY StoreID";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<Store> storeList = new List<Store>();
            while (reader.Read())
            {
                storeList.Add(
                new Store
                {
                   ID = reader.GetInt32(0),
                   StoreName = reader.GetString(1),
                   Area = reader.GetString(2),
                   Address = reader.GetString(3)
                }
                );
            }
            reader.Close();
            conn.Close();

            return storeList;
        }

        public int AddFoodItem(Item item)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO Item(ItemName,Status, Price, Category, StoreName, Dietary)
                                OUTPUT INSERTED.ItemId
                               VALUES(@name, @status, @price, @category, @storename, @dietary)";
            cmd.Parameters.AddWithValue("@name", item.Name);
            cmd.Parameters.AddWithValue("@status", item.Status);
            cmd.Parameters.AddWithValue("@price", item.Price);
            cmd.Parameters.AddWithValue("@category", item.Category);
            cmd.Parameters.AddWithValue("@storename", item.StoreName);
            if (item.Dietary == null)
            {
                cmd.Parameters.AddWithValue("@dietary", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@dietary", item.Dietary);

            }


            conn.Open();
            item.ItemID = (int)cmd.ExecuteScalar();
            conn.Close();
            return item.ItemID;

        }

        public void DeleteFoodItem(int itemid)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Item 
                                SET Status = 'Deleted'
                                where ItemId = @itemid";
            cmd.Parameters.AddWithValue("@itemid", itemid);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

        }

        public Item GetItem(int id)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Item WHERE ItemID = @id";
            cmd.Parameters.AddWithValue("id", id);
            Item item = new Item();

            conn.Open();

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                item.ItemID = reader.GetInt32(0);
                item.Name = reader.GetString(1);
                item.Status = reader.GetString(2);
                item.Price = reader.GetDecimal(3);
                item.Category = reader.GetString(4);
                item.Dietary = !reader.IsDBNull(5) ?
                            reader.GetString(5) : (string?)null;
                item.StoreName = !reader.IsDBNull(6) ?
                            reader.GetString(6) : (string?)null;
            }
            reader.Close();
            conn.Close();

            return item;
        }

        public void UpdateItem(Item item)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Item 
                                SET Status = @status, Price = @price, Dietary = @dietary
                                where ItemId = @itemid";
            cmd.Parameters.AddWithValue("@itemid", item.ItemID);
            cmd.Parameters.AddWithValue("@status", item.Status);
            cmd.Parameters.AddWithValue("@price", item.Price);
            if (item.Dietary == null)
            {
                cmd.Parameters.AddWithValue("@dietary", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@dietary", item.Dietary);

            }

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}
