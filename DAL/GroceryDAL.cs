﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebApplication.Models;

namespace WebApplication.DAL
{
    public class GroceryDAL
    {
        //=======================================================================================
        //Constructor
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public GroceryDAL()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("B_United_SG_ConnectionString");
            conn = new SqlConnection(strConn);
        }
        //=======================================================================================

        public List<Item> GetItems()
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Item WHERE Category = 'Grocery'";
            List<Item> ItemList = new List<Item>();
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                ItemList.Add(
                    new Item
                    {
                        ItemID = reader.GetInt32(0),
                        Name = reader.GetString(1),
                        Status = reader.GetString(2),
                        Price = reader.GetDecimal(3),
                        Category = reader.GetString(4),
                        Dietary = !reader.IsDBNull(5) ?
                                reader.GetString(5) : (string?)null,
                        StoreName = !reader.IsDBNull(5) ?
                                reader.GetString(5) : (string?)null,
                    }
                    );
            }
            reader.Close();
            conn.Close();

            return ItemList;
        }
    }
}
