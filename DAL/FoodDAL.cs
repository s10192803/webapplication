﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebApplication.Models;


namespace WebApplication.DAL
{
    public class FoodDAL
    {
        //=======================================================================================
        //Constructor
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public FoodDAL()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("B_United_SG_ConnectionString");
            conn = new SqlConnection(strConn);
        }
        //=======================================================================================

        public List<Store> GetAllStores()
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Store ORDER BY StoreID";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<Store> storeList = new List<Store>();
            while (reader.Read())
            {
                storeList.Add(

                    new Store
                    {
                        ID = reader.GetInt32(0),
                        StoreName = reader.GetString(1),
                        Area = reader.GetString(2),
                        Address = reader.GetString(3)
                    }
                    );
            }
            reader.Close();
            conn.Close();

            return storeList;
        }
        public Store GetStore(int StoreID)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Store WHERE StoreID = @StoreID";
            cmd.Parameters.AddWithValue("@StoreID", StoreID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            Store StoreDetails = new Store();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    StoreDetails.ID = reader.GetInt32(0);
                    StoreDetails.StoreName = reader.GetString(1);
                    StoreDetails.Area = reader.GetString(2);
                    StoreDetails.Address = reader.GetString(3);
                }
            }
            reader.Close();
            conn.Close();

            return StoreDetails;
        }
        public List<Item> GetFoodItems(string StoreName)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Item WHERE Category = 'Food' AND StoreName = @StoreName";
            cmd.Parameters.AddWithValue(@"StoreName", StoreName);
            List<Item> FoodItemList = new List<Item>();
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                FoodItemList.Add(
                    new Item
                    {
                        ItemID = reader.GetInt32(0),
                        Name = reader.GetString(1),
                        Status = reader.GetString(2),
                        Price = reader.GetDecimal(3),
                        Category = reader.GetString(4),
                        Dietary = !reader.IsDBNull(5) ?
                                reader.GetString(5) : (string?)null,
                        StoreName = reader.GetString(6)
                    }
                    );
            }
            reader.Close();
            conn.Close();

            return FoodItemList;
        }        
    }
}
