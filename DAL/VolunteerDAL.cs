﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebApplication.Models;


namespace WebApplication.DAL
{
    public class VolunteerDAL
    {
        //=======================================================================================
        //Constructor
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        public VolunteerDAL()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("B_United_SG_ConnectionString");
            conn = new SqlConnection(strConn);
        }
        //=======================================================================================
        public bool AuthenticateVolunteer(string email, string password)
        {

            bool authenticate = false;
            //Create a SqlCommand object and specify the SQL command
            //to get customer record to validate password
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT Password FROM Volunteer
                                WHERE EmailAddr=@emailEntered";
            cmd.Parameters.AddWithValue("@emailEntered", email);

            //open DB connection and execute SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows) //records found
            {
                while (reader.Read())
                {
                    if (reader.GetString(0) == password) //passwords match
                    {
                        authenticate = true;
                    }
                }
            }

            else
            {
                //no record found (no email)
            }
            reader.Close();
            conn.Close();

            return authenticate;
        }
        //=======================================================================================
        public bool ValidateVolunteerEmail(string email)
        {
            bool exist = false;

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT EmailAddr FROM Volunteer";

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if ((reader.GetString(0)).ToLower() == email.ToLower())
                    {
                        //There is an existing email
                        exist = true;
                        break;
                    }
                }
            }

            else
            {
                //no record found
                exist = false;
            }
            reader.Close();
            conn.Close();

            return exist;
        }
        //=======================================================================================
        public int Add(Staff volunteer)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO Volunteer (Name, EmailAddr, DOB, Gender, Password, PhoneNum)
                                OUTPUT INSERTED.VolunteerID
                                VALUES(@name, @email, @dob,@gender, @password, @phone)";
            cmd.Parameters.AddWithValue("@name", volunteer.Name);
            cmd.Parameters.AddWithValue("@email", volunteer.EmailAddr);
            cmd.Parameters.AddWithValue("@dob", volunteer.DOB);
            cmd.Parameters.AddWithValue("@gender", volunteer.Gender);
            cmd.Parameters.AddWithValue("@password", volunteer.Password);
            cmd.Parameters.AddWithValue("@phone", volunteer.PhoneNum);

            conn.Open();
            volunteer.ID = (int)cmd.ExecuteScalar();
            conn.Close();
            return volunteer.ID;
        }
        //=======================================================================================
        public List<Staff> GetAllVolunteer()
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Volunteer ORDER BY VolunteerID";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<Staff> StaffList = new List<Staff>();
            while (reader.Read())
            {
                StaffList.Add(
                new Staff
                {
                    ID = reader.GetInt32(0),
                    Name = reader.GetString(1),
                    EmailAddr = reader.GetString(2),
                    DOB = reader.GetDateTime(3),
                    PhoneNum = reader.GetString(4),
                    Gender = reader.GetString(5),
                    Password = reader.GetString(6),
                    Role = "Volunteer"
                }
                );
            }
            reader.Close();
            conn.Close();

            return StaffList;
        }
        //=======================================================================================
        public Staff GetDetails(string Email)
        {
            Staff volunteer = new Staff();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Volunteer
                                WHERE EmailAddr = @SelectedCustomerEmail";
            cmd.Parameters.AddWithValue("@SelectedCustomerEmail", Email);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    volunteer.ID = reader.GetInt32(0);
                    volunteer.Name = reader.GetString(1);
                    volunteer.EmailAddr = reader.GetString(2);
                    volunteer.DOB = reader.GetDateTime(3);
                    volunteer.PhoneNum = reader.GetString(4);
                    volunteer.Gender = reader.GetString(5);
                    volunteer.Password = reader.GetString(6);
                    volunteer.Role = "Volunteer";
                }
            }
            reader.Close();
            conn.Close();
            return volunteer;
        }
        //=======================================================================================
        public void ForgetPass(string email)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Volunteer SET Password=@Password 
                                WHERE EmailAddr=@email";
            cmd.Parameters.AddWithValue("@Password", "password");
            cmd.Parameters.AddWithValue("@email", email);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }
        //=======================================================================================
        public Staff GetDetailsByID(int ID)
        {
            Staff volunteer = new Staff();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Volunteer
                                WHERE VolunteerID = @ID";
            cmd.Parameters.AddWithValue("@ID", ID);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    volunteer.ID = reader.GetInt32(0);
                    volunteer.Name = reader.GetString(1);
                    volunteer.EmailAddr = reader.GetString(2);
                    volunteer.DOB = reader.GetDateTime(3);
                    volunteer.PhoneNum = reader.GetString(4);
                    volunteer.Gender = reader.GetString(5);
                    volunteer.Password = reader.GetString(6);
                    volunteer.Role = "Volunteer";
                }
            }
            reader.Close();
            conn.Close();
            return volunteer;
        }
        //=======================================================================================
        public int Edit(Staff staff)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE Volunteer 
                                SET Name=@Name, DOB=@DOB, PhoneNum=@PhoneNum, Password=@Pass
                                WHERE EmailAddr=@Email";
            cmd.Parameters.AddWithValue("@Name", staff.Name);
            cmd.Parameters.AddWithValue("@Email", staff.EmailAddr);
            cmd.Parameters.AddWithValue("@DOB", staff.DOB);
            cmd.Parameters.AddWithValue("@PhoneNum", staff.PhoneNum);
            cmd.Parameters.AddWithValue("@Pass", staff.Password);
            conn.Open();
            int count = (int)cmd.ExecuteNonQuery();
            conn.Close();
            return count;
        }
    }
}

